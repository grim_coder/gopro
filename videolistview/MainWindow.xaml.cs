﻿using Ninject;
using Ninject.Parameters;
using Prism.Events;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using MapControl;
using videolistview.Controls;
using videolistview.Utils;

namespace videolistview
{
	public partial class MainWindow : Window
	{
		internal IMainVM VM { get; set; }

		public MainWindow(IMainVM mainMV, IEventAggregator eventAggregator)
		{
			this.InitializeComponent();
			this.ChangeTheme(new Uri("Themes\\Generic.xaml", UriKind.Relative));
			this.VM = mainMV;
			this.DataContext = (object) this.VM;
			eventAggregator.GetEvent<FileSelected>().Subscribe((Action<IFile>) (file =>
			{
				this.MediaPlayer.Source = new Uri(file.Location);
				this.MediaPlayer.Play();
			}));
		}

		[Inject] public IKernel Kernel { get; set; }

		private void MapItemTouchDown(object sender, TouchEventArgs e)
		{
			MapItem mapItem = (MapItem) sender;
			mapItem.IsSelected = !mapItem.IsSelected;
			e.Handled = true;
		}

		private void EventSetter_OnHandler(object sender, MouseButtonEventArgs e)
		{
			MapItem mapItem = (MapItem) sender;
			mapItem.IsSelected = !mapItem.IsSelected;
			e.Handled = true;
		}

		private void UIElement_OnDrop(object sender, DragEventArgs e)
		{
			((IEnumerable<string>) (string[]) e.Data.GetData(DataFormats.FileDrop)).ToList<string>().ForEach(
				(Action<string>) (async name =>
				{
					if (this.VM.Files.Count<IFile>((Func<IFile, bool>) (file => file.Location == name)) > 0)
						return;
					IFile newFile = this.Kernel.Get<IFile>(Array.Empty<IParameter>());
					IFile file1 = newFile;
					List<Location> locationList = await newFile.GetLocation(name);
					file1.GPS = locationList;
					file1 = (IFile) null;
					locationList = (List<Location>) null;
					newFile.Location = name;
					newFile.FileName = ((IEnumerable<string>) name.Split('\\')).LastOrDefault<string>();
					this.VM.Files.Add(newFile);
				}));
		}

		private void UIElement_OnDragEnter(object sender, DragEventArgs e)
		{
		}

		private void ChangeTheme_OnChecked(object sender, RoutedEventArgs e)
		{
			App current = Application.Current as App;
			if ((sender as CheckBox).IsChecked.Value)
				this.ChangeTheme(new Uri("Themes\\dark.xaml", UriKind.Relative));
			else
				this.ChangeTheme(new Uri("Themes\\generic.xaml", UriKind.Relative));
		}

		public void ChangeTheme(Uri uri)
		{
			ResourceDictionary resourceDictionary = Application.LoadComponent(uri) as ResourceDictionary;
			Application.Current.Resources.MergedDictionaries.Clear();
			Application.Current.Resources.MergedDictionaries.Add(resourceDictionary);
		}

		private void MapViewBox_OnClick(object sender, RoutedEventArgs e)
		{
			if (this.mapControl.Visibility == Visibility.Visible)
			{
				this.ListView.Visibility = Visibility.Visible;
				this.mapControl.Visibility = Visibility.Hidden;
			}
			else
			{
				this.ListView.Visibility = Visibility.Hidden;
				this.mapControl.Visibility = Visibility.Visible;
			}
		}
	}
}