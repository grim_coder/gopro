﻿// Decompiled with JetBrains decompiler
// Type: videolistview.WpfNinjectBootstrapper
// Assembly: videolistview, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 98DB7602-AF6B-4D8B-AFED-DFCE1A57DD32
// Assembly location: C:\Users\taras\source\repos\videolistview\bin\Debug\videolistview.exe

using Ninject;
using Ninject.Parameters;
using Prism.Ninject;
using System;
using System.Windows;
using videolistview.Utils;

namespace videolistview
{
  public class WpfNinjectBootstrapper : NinjectBootstrapper
  {
    protected override void ConfigureKernel()
    {
      base.ConfigureKernel();
      this.Kernel.Bind<IMainVM>().To<MainMV>().InSingletonScope();
      this.Kernel.Bind<IFile>().To<File>();
    }

    protected override DependencyObject CreateShell()
    {
      return (DependencyObject) this.Kernel.Get<MainWindow>(Array.Empty<IParameter>());
    }

    protected override void InitializeModules()
    {
      base.InitializeModules();
    }

    protected override void InitializeShell()
    {
      base.InitializeShell();
      Application.Current.MainWindow = (Window) this.Shell;
      Application.Current.MainWindow.Show();
    }
  }
}
