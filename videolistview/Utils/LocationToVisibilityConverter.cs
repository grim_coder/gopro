﻿// Decompiled with JetBrains decompiler
// Type: videolistview.Utils.LocationToVisibilityConverter
// Assembly: videolistview, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 98DB7602-AF6B-4D8B-AFED-DFCE1A57DD32
// Assembly location: C:\Users\taras\source\repos\videolistview\bin\Debug\videolistview.exe

using MapControl;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace videolistview.Utils
{
  public class LocationToVisibilityConverter : IMultiValueConverter
  {
    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
      Visibility visibility = Visibility.Hidden;
      if (values.Length == 2 && values[0] is MapBase && values[1] is Point?)
      {
        MapBase mapBase = (MapBase) values[0];
        Point? nullable = (Point?) values[1];
        int num;
        if (nullable.HasValue)
        {
          Point point = nullable.Value;
          if (point.X >= 0.0)
          {
            point = nullable.Value;
            if (point.X <= mapBase.ActualWidth)
            {
              point = nullable.Value;
              if (point.Y >= 0.0)
              {
                point = nullable.Value;
                num = point.Y <= mapBase.ActualHeight ? 1 : 0;
                goto label_7;
              }
            }
          }
        }
        num = 0;
label_7:
        if (num != 0)
          visibility = Visibility.Visible;
      }
      return (object) visibility;
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      throw new NotSupportedException();
    }
  }
}
