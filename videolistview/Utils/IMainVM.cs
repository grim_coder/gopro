﻿// Decompiled with JetBrains decompiler
// Type: videolistview.Utils.IMainVM
// Assembly: videolistview, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 98DB7602-AF6B-4D8B-AFED-DFCE1A57DD32
// Assembly location: C:\Users\taras\source\repos\videolistview\bin\Debug\videolistview.exe

using System.Collections.ObjectModel;

namespace videolistview.Utils
{
  public interface IMainVM
  {
    ObservableCollection<IFile> Files { get; set; }
  }
}
