﻿
using Ninject;
using Ninject.Parameters;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MapControl;
using videolistview.Controls;

namespace videolistview.Utils
{
  internal class MainMV2 : BaseVM, IMainVM
  {
    [Inject]
    public IKernel Kernel { get; set; }

    public MainMV2(IKernel Kernel, IEventAggregator eventAggregator)
    {
      this.Files = new ObservableCollection<IFile>();
      IFile file1 = Kernel.Get<IFile>(Array.Empty<IParameter>());
      file1.Location = "C:\\Users\\taras\\source\\repos\\gpmf-parser-master\\samples\\hero6.mp4";
      file1.FileName = "hero6.mp4";
      file1.GPS = new List<Location>()
      {
        new Location() { Latitude = 33.0, Longitude = -112.0 }
      };
      this.Files.Add(file1);
      eventAggregator.GetEvent<FileSelected>().Subscribe((Action<IFile>) (file => this.SelectedFile = file));
    }

    public ObservableCollection<IFile> Files { get; set; }
  }
}
