﻿// Decompiled with JetBrains decompiler
// Type: videolistview.Utils.IFile
// Assembly: videolistview, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 98DB7602-AF6B-4D8B-AFED-DFCE1A57DD32
// Assembly location: C:\Users\taras\source\repos\videolistview\bin\Debug\videolistview.exe

using System.Collections.Generic;
using System.Threading.Tasks;

namespace videolistview.Utils
{
  public interface IFile
  {
    List<MapControl.Location> GPS { get; set; }

    string FileName { get; set; }

    string Location { get; set; }

    void WasSelected();

    Task<List<MapControl.Location>> GetLocation(string name);
  }
}
