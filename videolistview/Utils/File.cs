﻿// Decompiled with JetBrains decompiler
// Type: videolistview.Utils.File
// Assembly: videolistview, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 98DB7602-AF6B-4D8B-AFED-DFCE1A57DD32
// Assembly location: C:\Users\taras\source\repos\videolistview\bin\Debug\videolistview.exe

using Ninject;
using Prism.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using videolistview.Controls;

namespace videolistview.Utils
{
  public class File : BaseVM, IFile
  {
    private string _location;
    private string _fileName;
    private List<MapControl.Location> _gps1;

    [Inject]
    public IEventAggregator EventAggregator { get; set; }

    public File()
    {
      this.GPS = new List<MapControl.Location>();
    }

    public string FileName
    {
      get
      {
        return this._fileName;
      }
      set
      {
        this._fileName = value;
        this.OnPropertyChanged(nameof (FileName));
      }
    }

    public string Location
    {
      get
      {
        return this._location;
      }
      set
      {
        this._location = value;
        this.OnPropertyChanged(nameof (Location));
      }
    }

    public List<MapControl.Location> GPS
    {
      get
      {
        return this._gps1;
      }
      set
      {
        this._gps1 = value;
        this.OnPropertyChanged(nameof (GPS));
      }
    }

    public MapControl.Location GpsFirst
    {
      get
      {
        return this.GPS.FirstOrDefault<MapControl.Location>();
      }
    }

    public void WasSelected()
    {
      this.EventAggregator.GetEvent<FileSelected>().Publish((IFile) this);
    }

    public async Task<List<MapControl.Location>> GetLocation(string location)
    {
      List<string> source = await new videolistview.Utils.Utils().RunProcessAsync("gpmfdemo.exe", location);
      return source.Where<string>((Func<string, bool>) (s =>
      {
        if (!string.IsNullOrEmpty(s) && s.Contains("GPS"))
          return s.Contains("deg");
        return false;
      })).Select<string, string[]>((Func<string, string[]>) (s => ((IEnumerable<string>) s.Split(',')).Select<string, string>((Func<string, string>) (s1 => s1.Replace("deg", ""))).Select<string, string>((Func<string, string>) (s1 => s1.Replace("GPS5 ", ""))).Take<string>(2).ToArray<string>())).Select<string[], MapControl.Location>((Func<string[], MapControl.Location>) (strings => new MapControl.Location()
      {
        Latitude = double.Parse(strings[0]),
        Longitude = double.Parse(strings[1])
      })).ToList<MapControl.Location>();
    }
  }
}
