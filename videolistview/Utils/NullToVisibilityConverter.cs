﻿// Decompiled with JetBrains decompiler
// Type: videolistview.Utils.NullVisibilityConverter
// Assembly: videolistview, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 98DB7602-AF6B-4D8B-AFED-DFCE1A57DD32
// Assembly location: C:\Users\taras\source\repos\videolistview\bin\Debug\videolistview.exe

using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace videolistview.Utils
{
  public class NullVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return (object) (Visibility) (value == null ? 1 : 0);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
