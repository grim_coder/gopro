﻿// Decompiled with JetBrains decompiler
// Type: videolistview.Utils.BaseVM
// Assembly: videolistview, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 98DB7602-AF6B-4D8B-AFED-DFCE1A57DD32
// Assembly location: C:\Users\taras\source\repos\videolistview\bin\Debug\videolistview.exe

using MapControl;
using System.ComponentModel;

namespace videolistview.Utils
{
  public class BaseVM : INotifyPropertyChanged
  {
    private IFile _selectedFileGeo;

    public event PropertyChangedEventHandler PropertyChanged;

    public void OnPropertyChanged(string propertyName)
    {
      // ISSUE: reference to a compiler-generated field
      PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
      if (propertyChanged == null)
        return;
      propertyChanged((object) this, new PropertyChangedEventArgs(propertyName));
    }

    public BingMapsTileLayer MapLayer
    {
      get
      {
        BingMapsTileLayer bingMapsTileLayer = new BingMapsTileLayer();
        bingMapsTileLayer.SourceName = "Bing Maps Road";
        bingMapsTileLayer.Description = "© [Microsoft](http://www.bing.com/maps/)";
        bingMapsTileLayer.Mode = BingMapsTileLayer.MapMode.Road;
        return bingMapsTileLayer;
      }
    }

    public IFile SelectedFile
    {
      get
      {
        return this._selectedFileGeo;
      }
      set
      {
        this._selectedFileGeo = value;
        this.OnPropertyChanged(nameof (SelectedFile));
      }
    }
  }
}
