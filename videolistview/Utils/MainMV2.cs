﻿// Decompiled with JetBrains decompiler
// Type: videolistview.Utils.MainMV
// Assembly: videolistview, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 98DB7602-AF6B-4D8B-AFED-DFCE1A57DD32
// Assembly location: C:\Users\taras\source\repos\videolistview\bin\Debug\videolistview.exe

using Ninject;
using Prism.Events;
using System;
using System.Collections.ObjectModel;
using videolistview.Controls;

namespace videolistview.Utils
{
  internal class MainMV : BaseVM, IMainVM
  {
    [Inject]
    public IKernel Kernel { get; set; }

    public MainMV(IKernel Kernel, IEventAggregator eventAggregator)
    {
      this.Files = new ObservableCollection<IFile>();
      eventAggregator.GetEvent<FileSelected>().Subscribe((Action<IFile>) (file => this.SelectedFile = file));
    }

    public ObservableCollection<IFile> Files { get; set; }
  }
}
