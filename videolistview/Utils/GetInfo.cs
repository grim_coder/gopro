﻿// Decompiled with JetBrains decompiler
// Type: videolistview.Utils.Utils
// Assembly: videolistview, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 98DB7602-AF6B-4D8B-AFED-DFCE1A57DD32
// Assembly location: C:\Users\taras\source\repos\videolistview\bin\Debug\videolistview.exe

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace videolistview.Utils
{
  internal class Utils
  {
    public async Task<List<string>> RunProcessAsync(string fileName, string args)
    {
      List<string> stringList1;
      using (Process process = new Process()
      {
        StartInfo = {
          FileName = fileName,
          Arguments = args,
          UseShellExecute = false,
          CreateNoWindow = true,
          RedirectStandardOutput = true,
          RedirectStandardError = true
        },
        EnableRaisingEvents = true
      })
      {
        List<string> stringList = await this.RunProcessAsync(process).ConfigureAwait(false);
        stringList1 = stringList;
      }
      return stringList1;
    }

    private Task<List<string>> RunProcessAsync(Process process)
    {
      TaskCompletionSource<List<string>> tcs = new TaskCompletionSource<List<string>>();
      List<string> result = new List<string>();
      process.Exited += (EventHandler) ((s, ea) => tcs.SetResult(result));
      process.OutputDataReceived += (DataReceivedEventHandler) ((s, ea) => result.Add(ea.Data));
      process.ErrorDataReceived += (DataReceivedEventHandler) ((s, ea) => Console.WriteLine("ERR: " + ea.Data));
      if (!process.Start())
        throw new InvalidOperationException("Could not start process: " + (object) process);
      process.BeginOutputReadLine();
      process.BeginErrorReadLine();
      return tcs.Task;
    }
  }
}
