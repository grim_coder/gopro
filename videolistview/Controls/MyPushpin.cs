﻿// Decompiled with JetBrains decompiler
// Type: videolistview.Controls.MyPushpin
// Assembly: videolistview, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 98DB7602-AF6B-4D8B-AFED-DFCE1A57DD32
// Assembly location: C:\Users\taras\source\repos\videolistview\bin\Debug\videolistview.exe

using MapControl;
using System.Windows.Input;
using videolistview.Utils;

namespace videolistview.Controls
{
  public class MyPushpin : Pushpin
  {
    protected override void OnMouseUp(MouseButtonEventArgs e)
    {
      (this.DataContext as IFile)?.WasSelected();
    }
  }
}
