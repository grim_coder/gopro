﻿

using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using videolistview.Utils;

namespace videolistview.Controls
{
  public class File : Control
  {
    static File()
    {
      FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof (File), (PropertyMetadata) new FrameworkPropertyMetadata((object) typeof (File)));
    }

    protected override void OnMouseUp(MouseButtonEventArgs e)
    {
      (this.DataContext as IFile)?.WasSelected();
    }
  }
}
